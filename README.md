# Corona Google Analytis module

## How to use it

**Load and initialize GA in `main.lua` with Your GA Tracking ID:**
```lua
ga = require( "ga" )
ga:init( "UA-12345678-9" )
```

**Send screen views:**
```lua
function scene:enterScene( event )
  ga:view("ScreenName")
  
  local group = self.view
  -- rest of code
end
```
Screen name is for example: menu, game, score, ...

**Send custom events:**
```lua
ga:event("category", "action", "label", value)
```
For example: `ga:event("game", "win", "score", 123)`, `ga:event("button", "click", "share", 0)`.

Find more details about parameters on [developers.google.com](https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#apptracking) page.

## Issues
* cache data when network is not available